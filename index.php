<?php
header("Content-type: json/application; charset=utf-8 ");
//header("Content-Type:text/html; charset=utf-8");

spl_autoload_register(function ($class) {
    include "core/{$class}.php";
});


$q = trim(strip_tags($_GET['q']));
$params = explode("/", $q);
$id = $params[0];

if ($id != "") {
    $data = ApiData::testMethodDetails($id);

} else {
    $data = ApiData::testMethodSelect();

}

echo json_encode($data);